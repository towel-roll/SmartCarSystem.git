#ifndef SETTIME_H
#define SETTIME_H

#include <QMainWindow>
#include <QDebug>
#include <QDateTime>
#include <QListWidgetItem>

#include "statusbar.h"
#include "backoff.h"

#define GEC6818_K4_STA		_IOR('K', 3, unsigned long)

extern "C"
{
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
}

namespace Ui {
class SetTime;
}

class SetTime : public QMainWindow
{
    Q_OBJECT

public:
    explicit SetTime(QWidget *parent = nullptr);
    ~SetTime();

private slots:
    void status();

    void on_yes_clicked();

    void on_no_clicked();

    void showEvent(QShowEvent *event);                              //重写显示事件

    void hideEvent(QHideEvent *event);                              //重写隐藏事件

    void back();                                                    //模拟汽车挂挡，判断是否开启倒车辅助

private:
    Ui::SetTime *ui;

    StatusBar *s;

    QListWidgetItem  *it_s;

    QTimer *t_status;

    int key_fd = -1;                    //键盘驱动文件标识符

    QTimer *t_back;                     //back函数的定时器

    int flag_event = 0;                 //事件标志位
};

#endif // SETTIME_H
