#ifndef MONITOR_H
#define MONITOR_H

#include <QMainWindow>
#include <QDebug>
#include <QTimer>
#include <QEvent>

#include "statusbar.h"
#include "backoff.h"

extern "C"{
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
}

#define GEC6818_K4_STA		_IOR('K', 3, unsigned long)

namespace Ui {
class Monitor;
}

class Monitor : public QMainWindow
{
    Q_OBJECT

public:
    explicit Monitor(QWidget *parent = nullptr);
    ~Monitor();

private slots:
    void status();

    void video();

    void showEvent(QShowEvent *event);

    void hideEvent(QHideEvent *event);

    void back();
private:
    Ui::Monitor *ui;

    StatusBar *s;

    QTimer *t_status;

    QTimer *t_video;

    FrameBuffer frame;

    int key_fd = -1;

    QTimer *t_back;

    int flag_event = 0;

    int vfd;
};

#endif // MONITOR_H
