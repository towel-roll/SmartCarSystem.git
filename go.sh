#!/bin/sh
#Program:
#		安装车载系统所需的驱动并执行可执行文件运行程序
#History:
#2020/3/23	Towel_Roll	First release

insmod /Car/driver/sr04_drv.ko
insmod /Car/driver/key_drv.ko

chmod 777 /Car/Car_system
/Car/Car_system