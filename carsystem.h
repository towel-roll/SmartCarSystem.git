#ifndef CARSYSTEM_H
#define CARSYSTEM_H

#include <QMainWindow>
#include <QTimer>
#include <QTime>
#include <QEvent>
#include <QSqlDatabase>
#include <QSqlQuery>

#include "weather.h"
#include "statusbar.h"
#include "music.h"
#include "monitor.h"
#include "setup.h"
#include "backoff.h"


#define GEC6818_K4_STA		_IOR('K', 3, unsigned long)

extern "C"
{
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
}

QT_BEGIN_NAMESPACE
namespace Ui { class CarSystem; }
QT_END_NAMESPACE

class CarSystem : public QMainWindow
{
    Q_OBJECT

public:
    CarSystem(QWidget *parent = nullptr);
    ~CarSystem();

private slots:
    void on_Music_bt_clicked();

    void on_Monitor_bt_clicked();

    void on_Set_up_bt_clicked();

    //virtual void changeEvent(QEvent *);
    void changeEvent(QEvent *e);

    void showEvent(QShowEvent *event);

    void hideEvent(QHideEvent *event);

    void back();

private:
    Ui::CarSystem *ui;

    int key_fd = -1;

    QTimer *t_back;

    int flag_event = 0;

    QSqlDatabase db_l;
};
#endif // CARSYSTEM_H
