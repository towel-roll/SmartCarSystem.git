#include "setup.h"
#include "ui_setup.h"

Setup::Setup(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Setup)
{
    ui->setupUi(this);

    /****************************************************************************/
    t_status = new QTimer(this);
    connect(t_status,SIGNAL(timeout()),this,SLOT(status()));
    t_status->start(500);

    //新建一个窗体 statusbar
    s = new  StatusBar(this);
    //创建一项信息
    it_s = new QListWidgetItem();
    //消除选中效果
    it_s->setFlags(it_s->flags() & ~Qt::ItemIsSelectable);
    //把该项信息 设置到 列表中
    ui->StatusBar_lw->addItem(it_s);
    //设置IT  信息项的大小    (根据窗体大小来设置  it 项的大小)
    it_s->setSizeHint(s->sizeHint());
    //为该选 信息添加 窗体
    ui->StatusBar_lw->setItemWidget(it_s,s);

    /*************************QT 语言家********************************************/
    trans = new QTranslator(this);

    /****************************************************************************/
    t_back = new QTimer(this);

    key_fd = ::open("/dev/my_key", O_RDWR);

    if(key_fd<0)
        perror("back_fd error");

    connect(t_back,SIGNAL(timeout()),this,SLOT(back()));

    t_back->start(1000);
}

Setup::~Setup()
{   
    t_back->stop();

    delete t_back;

    ::close(key_fd);

    delete trans;

    t_status->stop();

    delete t_status;

    delete it_s;

    delete s;

    delete ui;

    qDebug()<<"~Setup"<<endl;
}

void Setup::status()
{
    if(s->flag_return == 1)
    {
        s->flag_return = 0;

        flag_event = 2;

        this->close();
        this->parentWidget()->show();
    }
}


void Setup::on_Language_clicked()
{
    Language *w  = new Language(this);
    w->show();
    this->hide();
}

void Setup::changeEvent(QEvent *e)
{
    if(e->type() == QEvent::LanguageChange)
    {
        this->ui->retranslateUi(this);
    }
}

void Setup::on_Time_clicked()
{
    SetTime *w  = new SetTime(this);
    w->show();
    this->hide();
}

void Setup::showEvent(QShowEvent *)
{
    if(flag_event == 1)
    {
        qDebug()<<"Setup show"<<endl;

        flag_event = 0;

        //开启驱动文件夹
        key_fd = ::open("/dev/my_key", O_RDWR);

        if(key_fd<0)
            perror("back_fd error");

        t_back->start(1000);

        //开启定时器
        t_status->start(500);
    }
}

void Setup::hideEvent(QHideEvent *)
{
    if(flag_event == 2)
    {
        qDebug()<<"Setup hide"<<endl;

        flag_event = 1;

        //关闭定时器
        t_back->stop();

        //关闭驱动文件
        ::close(key_fd);

        //关闭定时器
        t_status->stop();
    }
}

void Setup::back()
{
    int val = -1;
    //读取按键的状态
    int rt = ioctl(key_fd, GEC6818_K4_STA, &val);

    if(rt == 0)
    {
       if(val == 1)
       {
           qDebug()<<"K4 enter"<<endl;

           flag_event = 2;

           Backoff *w = new Backoff(this);
           w->show();

           this->hide();
       }
    }
}
