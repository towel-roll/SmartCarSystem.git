#ifndef STATUSBAR_H
#define STATUSBAR_H

#include <QWidget>
#include <QTimer>
#include <QTime>
#include <QDebug>


namespace Ui {
class StatusBar;
}

class StatusBar : public QWidget
{
    Q_OBJECT

public:
    int flag_return = 0;
    explicit StatusBar(QWidget *parent = nullptr);
    ~StatusBar();

private slots:
    void show_time();

    void on_Return_bt_clicked();

private:
    Ui::StatusBar *ui;

    QTimer *time;
};

#endif // STATUSBAR_H
