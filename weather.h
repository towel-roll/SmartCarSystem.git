#ifndef WEATHER_H
#define WEATHER_H

#include <QWidget>
#include <QTimer>
#include <QDateTime>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonParseError>

namespace Ui {
class Weather;
}

#define GD_WEATHER_KEY  QString("****")
#define CITY           QString("440100")

class Weather : public QWidget
{
    Q_OBJECT

public:
    explicit Weather(QWidget *parent = nullptr);
    ~Weather();

private slots:
    void show_time();

    void readdata(QNetworkReply *);

    void wea_image(QString image);

    void select_image(QString type);
private:
    Ui::Weather *ui;

    QTimer *time;

    QNetworkAccessManager *Network;

    QString weather_api;
};

#endif // WEATHER_H
