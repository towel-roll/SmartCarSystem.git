#ifndef MUSIC_H
#define MUSIC_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QDebug>
#include <QTimer>
#include <QProcess>
#include <QFileDialog>
#include <QDir>

#include "statusbar.h"
#include "backoff.h"

#define GEC6818_K4_STA		_IOR('K', 3, unsigned long)

extern "C"
{
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
}

namespace Ui {
class Music;
}

class Music : public QMainWindow
{
    Q_OBJECT

public:
    explicit Music(QWidget *parent = nullptr);
    ~Music();

    void playmusic(int num);

private slots:
    void status();

    void update();

    void readdata();

    void on_play_bt_clicked();

    void on_dir_bt_clicked();

    void on_music_lw_itemDoubleClicked(QListWidgetItem *item);

    void on_left_bt_clicked();

    void on_right_bt_clicked();

    void on_horizontalSlider_valueChanged(int value);

    void on_horizontalSlider_sliderPressed();

    void on_horizontalSlider_sliderReleased();

    void on_volume_bt_clicked();

    void on_horizontalSlider_2_valueChanged(int value);

    void on_order_cb_currentTextChanged(const QString &arg1);

    void showEvent(QShowEvent *event);                              //重写显示事件

    void hideEvent(QHideEvent *event);                              //重写隐藏事件

    void back();                                                    //模拟汽车挂挡，判断是否开启倒车辅助

    void on_lrc_bt_clicked();

private:
    Ui::Music *ui;

    StatusBar *s;

    QListWidgetItem  *it_s;

    QTimer *t_status;

    QTimer *t_music;

    QProcess *process_music;

    QStringList StrListMusic;           //保存歌曲路径

    QFile LrcFile;                      //歌词文件

    int number = 0;                     //当前播放的歌曲序号

    int sl_value = 0;                   //滑动条值

    int flag_play = 0;                  //音乐播放标志 1：播放中 0:没有播放

    int flag_way = 0;                   //播放方式标志

    int key_fd = -1;                    //键盘驱动文件标识符

    QTimer *t_back;                     //back函数的定时器

    int flag_event = 0;                 //事件标志位
};

#endif // MUSIC_H
