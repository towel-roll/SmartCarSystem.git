#ifndef SETUP_H
#define SETUP_H

#include <QMainWindow>
#include <QDebug>
#include <QTimer>
#include <QTranslator>
#include <QEvent>
#include <QListWidgetItem>

#include "statusbar.h"
#include "language.h"
#include "settime.h"
#include "backoff.h"

#define GEC6818_K4_STA		_IOR('K', 3, unsigned long)

extern "C"
{
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
}

namespace Ui {
class Setup;
}

class Setup : public QMainWindow
{
    Q_OBJECT

public:
    explicit Setup(QWidget *parent = nullptr);
    ~Setup();


private slots:
    void status();

    void on_Language_clicked();

    void changeEvent(QEvent *e);

    void on_Time_clicked();

    void showEvent(QShowEvent *event);                              //重写显示事件

    void hideEvent(QHideEvent *event);                              //重写隐藏事件

    void back();                                                    //模拟汽车挂挡，判断是否开启倒车辅助

private:
    Ui::Setup *ui;

    StatusBar *s;

    QListWidgetItem  *it_s;

    QTimer *t_status;

    QTranslator *trans;

    int key_fd = -1;                    //键盘驱动文件标识符

    QTimer *t_back;                     //back函数的定时器

    int flag_event = 0;                 //事件标志位
};

#endif // SETUP_H
