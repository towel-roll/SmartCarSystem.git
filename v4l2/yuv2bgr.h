#ifndef _YUV2BGR_H_
#define _YUV2BGR_H_

#include <stdbool.h>

#define uint8_t unsigned char 
#define uint32_t unsigned int 

typedef struct{
    int x;
    int y;
    int w;
    int h;
}T_Rect;

bool Yuv422ToBgr24(unsigned char *yuyvdata, unsigned char *bgrdata, int picw, int pich);
void DrawRect(char *srcRgb, int srcW, int srcH, int srcC, T_Rect *rect, uint8_t color);

#endif