#ifndef _V4L2_H_
#define _V4L2_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdbool.h>
#include <time.h>
#include <sys/time.h>

#define uint8_t unsigned char

#define COUNT 4
//保存映射空首地址+长度
struct VideoFrame
{
	char *start;
	int length;
};

typedef struct FrameBuffer
{
    unsigned char buf[7372800];
    long long length;
}FrameBuffer;

int video_linux_init(const char *device, unsigned short width, unsigned short height);
uint8_t video_linux_mmap(int vfd);
uint8_t video_linux_start(int vfd);
uint8_t video_linux_get_frame(int vfd, FrameBuffer *frame);
uint8_t video_linux_stop(int vfd);
void video_linux_destroy(int vfd);

bool yuyv_to_bgr24(unsigned char *yuyvdata, unsigned char *bgrdata, int picw, int pich);//计算

void init_yuv422p_table(void);
void yuyvpacked_to_bgr24(unsigned char* yuyv, unsigned char* bgr, int width, int height);//查表

#endif
