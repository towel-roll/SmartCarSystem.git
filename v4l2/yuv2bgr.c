#include "yuv2bgr.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


uint8_t *auto_color_table;
uint8_t *color_table;
int *colortab;
int *u_b_tab;
int *u_g_tab;
int *v_g_tab;
int *v_r_tab;
int *y_coeff;

// color yuv
static uint8_t color_yuv[6][3] = {
    {0x4C, 0x55, 0xFF},//RED
    {0x00, 0x80, 0x80},//BLACK
    {0xFF, 0x80, 0x80},//WHITE
    {0xB3, 0xAB, 0x00},//BLUE
    {0x1D, 0xFF, 0x6B},//GREEN
    {0xE2, 0x00, 0x95},//YELLOW
};

static uint8_t m_color = 2;

//运算
bool Yuv422ToBgr24(unsigned char *yuyvdata, unsigned char *bgrdata, int picw, int pich)
{
    int i, j;
    unsigned char y1,y2,u,v;
    int r1,g1,b1,r2,g2,b2;
    int offset;
    int offset_rgb;
    int temp;

    //确保所转的数据或要保存的地址有效
    if(yuyvdata == NULL || bgrdata == NULL)
    {
        return false;
    }

    int tmpw = picw>>1;
    for(i=0; i<pich; i++)
    {
        offset = i*tmpw;
        for(j=0; j<tmpw; j++)//
        {
            //yuv422
            //R = 1.164*(Y-16) + 1.159*(V-128);
            //G = 1.164*(Y-16) - 0.380*(U-128)+ 0.813*(V-128);
            //B = 1.164*(Y-16) + 2.018*(U-128));

            //下面的四个像素为：[Y0 U0 V0] [Y1 U1 V1] -------------[Y2 U2 V2] [Y3 U3 V3]
            //存放的码流为：    Y0 U0 Y1 V1------------------------Y2 U2 Y3 V3
            //映射出像素点为：  [Y0 U0 V1] [Y1 U0 V1]--------------[Y2 U2 V3] [Y3 U2 V3]

            //获取每个像素yuyv数据   YuYv
			temp = offset<<2;
            y1 = *(yuyvdata + temp );                //yuv像素的Y
            u  = *(yuyvdata + temp+1);              //yuv像素的U
            y2 = *(yuyvdata + temp+2);
            v  = *(yuyvdata + temp+3);

            //把yuyv数据转换为rgb数据
            r1 = y1 + ((267*(v-128))>>8);
            g1 = y1 - ((88*(u-128))>>8);
            b1 = y1 + ((454*(u-128))>>8);

            r2 = y2 + ((267*(v-128))>>8);
            g2 = y2 - ((88*(u-128))>>8);
            b2 = y2 + ((454*(u-128))>>8);

//            r1 = y1 + ( (((v-128)<<8) + ((v-128)<<3) + ((v-128)<<1) + (v-128)) >>8);
//            g1 = y1 - ( (((u-128)<<6) + ((u-128)<<4) + ((u-128)<<3)          ) >>8);
//            b1 = y1 + ( (((u-128)<<8) + ((u-128)<<7) + ((u-128)<<6) + ((u-128)<<2) + ((u-128)<<1)) >>8);

//            r2 = y2 + ( (((v-128)<<8) + ((v-128)<<3) + ((v-128)<<1) + (v-128)) >>8);
//            g2 = y2 - ( (((u-128)<<6) + ((u-128)<<4) + ((u-128)<<3)          ) >>8);
//            b2 = y2 + ( (((u-128)<<8) + ((u-128)<<7) + ((u-128)<<6) + ((u-128)<<2) + ((u-128)<<1)) >>8);

            if(r1 > 255) r1=255;
            else if(r1 < 0) r1 = 0;

            if(g1 > 255) g1=255;
            else if(g1 < 0) g1 = 0;

            if(b1 > 255) b1=255;
            else if(b1 < 0) b1 = 0;

            if(r2 > 255) r2=255;
            else if(r2 < 0) r2 = 0;

            if(g2 > 255) g2=255;
            else if(g2 < 0) g2 = 0;

            if(b2 > 255) b2=255;
            else if(b2 < 0) b2 = 0;

            //把rgb值保存于rgb空间 数据为反向
            offset_rgb = (offset<<2) + (offset<<1);
            bgrdata[offset_rgb]     = (unsigned char)b1;
            bgrdata[offset_rgb + 1] = (unsigned char)g1;
            bgrdata[offset_rgb + 2] = (unsigned char)r1;
            bgrdata[offset_rgb + 3] = (unsigned char)b2;
            bgrdata[offset_rgb + 4] = (unsigned char)g2;
            bgrdata[offset_rgb + 5] = (unsigned char)r2;

            offset++;
        }
    }
    return true;
}


// 传入imgData 是 YUV420格式
static int _visionDrawRect(char *imgData,int iWidth, int iHeight,int startX, int startY, int endX, int endY)
{
    int lineX1, lineX2, lineY1, lineY2;
    int nLoop;
    int iWidth_size = iWidth<<1;
    if (NULL == imgData || endX < 0 || endY < 0 
    || startX < 0 || startY < 0 || iWidth < 0 || iHeight < 0)
    {
        return -1;
    }

 //   if (startX == endX)
    {   
        lineX1 = startX; 
        lineY1 = startY;
        lineY2 = endY;
        if (startY > endY)
        {
           lineY1 = endY;
           lineY2 = startY;
        }

        // for(int i=100;i<200;i++)
        // {
        //     imgData[10*iWidth*2 + i] = 1;
        // }
        int startX_temp = startX<<1;
        int endX_temp   = endX<<1;
        
        for (nLoop = lineY1; nLoop < lineY2; nLoop++)
        {
            imgData[nLoop*iWidth_size + startX_temp] = color_yuv[m_color][0];//Y
            imgData[nLoop*iWidth_size + startX_temp+1] = color_yuv[m_color][1];//U
            imgData[nLoop*iWidth_size + startX_temp+2] = color_yuv[m_color][0];//Y
            imgData[nLoop*iWidth_size + startX_temp+3] = color_yuv[m_color][2];//V
            // imgData[nLoop*iWidth*2 + startX+2] = 0;
            // imgData[nLoop*iWidth*2 + startX+3] = 1;
            
            imgData[nLoop*iWidth_size + endX_temp] = color_yuv[m_color][0];//Y
            imgData[nLoop*iWidth_size + endX_temp+1] = color_yuv[m_color][1];//U
            imgData[nLoop*iWidth_size + endX_temp+2] = color_yuv[m_color][0];//Y
            imgData[nLoop*iWidth_size + endX_temp+3] = color_yuv[m_color][2];//V
        }
    }
   // else if(startY == endY)
    {
        lineY1 = startY; 
        lineX1 = startX;
        lineX2 = endX;
        if (startX > endX)
        {
           lineX1 = endX;
           lineX2 = startX;
        }
        for (nLoop = lineX1>>1; nLoop < lineX2>>1; nLoop++)
        {
            int temp = nLoop<<2;

            imgData[lineY1*iWidth_size + temp] = color_yuv[m_color][0];
            imgData[lineY1*iWidth_size + temp+1] = color_yuv[m_color][1];
            imgData[lineY1*iWidth_size + temp+2] = color_yuv[m_color][0];
            imgData[lineY1*iWidth_size + temp+3] = color_yuv[m_color][2];
            
            imgData[endY*iWidth_size + temp] = color_yuv[m_color][0];
            imgData[endY*iWidth_size + temp+1] = color_yuv[m_color][1];
            imgData[endY*iWidth_size + temp+2] = color_yuv[m_color][0];
            imgData[endY*iWidth_size + temp+3] = color_yuv[m_color][2];
        }
    }/*
    else
    {
        return -1;
    }*/
    return 0;
}

//draw rect
void DrawRect(char *srcRgb, int srcW, int srcH, int srcC, T_Rect *rect, uint8_t color)
{
	int left = rect->x;
	int top = rect->y;
	int right = rect->x + rect->w;
	int bot = rect->y + rect->h;

    m_color = color;

	_visionDrawRect(srcRgb, srcW, srcH, left, top, right, bot);
}
