//
#include "v4l2.h"

struct VideoFrame MFrame[COUNT];//定义保存映射空间的首地址和长度

int video_linux_init(const char *device, unsigned short width, unsigned short height)
{
	printf("%s %d %d \n", device, width, height);
	//1.打开摄像头设备文件
    int vfd = open(device,O_RDWR);
    if(vfd < 0)
    {
            perror("Video device open!");
            return -1;
    }


	//2.设置摄像头格式
	struct v4l2_format sfmt;
	sfmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    	sfmt.fmt.pix.width = width;
    	sfmt.fmt.pix.height = height;
    	sfmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;//（设置视频输出格式，但是要摄像头支持）
	int sret = ioctl(vfd, VIDIOC_S_FMT, &sfmt);
	if(sret < 0)
	{
		perror("set fmt fail");
		close(vfd);
		return -1;
	}

	//3.获取摄像头当前的采集格式
	struct v4l2_format vfmt;
	vfmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	int ret = ioctl(vfd, VIDIOC_G_FMT, &vfmt);
	if(ret < 0)
	{
		perror("Get FMT fail!");
		exit(1);	
	}

	char fmtstr[8] = {0};
	memcpy(fmtstr,&vfmt.fmt.pix.pixelformat,4);
	
	if(strcmp(fmtstr,"YUYV") != 0)
	{
		close(vfd);
		return -1;
	}

	printf("width:%d,height:%d,format:%s\n",vfmt.fmt.pix.width,vfmt.fmt.pix.height,(char *)fmtstr);

    return vfd;
}

uint8_t video_linux_mmap(int vfd)
{
	memset(MFrame, 0, sizeof(MFrame));
	//向内核申请视频缓存
	struct v4l2_requestbuffers reqbuf;
	memset(&reqbuf, 0, sizeof(reqbuf));
	reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	reqbuf.count = COUNT;//申请缓存区个数
	reqbuf.memory = V4L2_MEMORY_MMAP;
	int ret = ioctl(vfd, VIDIOC_REQBUFS, &reqbuf);
	if(ret < 0)
	{
		perror("request fail");	
        return 0;
	}

	//把内核空间映射到用户空间
	struct v4l2_buffer kbuf;
	memset(&kbuf, 0, sizeof(kbuf));
	int i = 0;
    for(i=0; i<(int)reqbuf.count; i++)
	{
		//把申请的缓存区空间一个一个取出来与用户空间映射
		kbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		kbuf.index = i;
		kbuf.memory = V4L2_MEMORY_MMAP;
		if(ioctl(vfd, VIDIOC_QUERYBUF, &kbuf)<0)
		{
			perror("query buf fail");
            return 0;
		}	
		
		//映射
		MFrame[i].length = kbuf.length;
		MFrame[i].start = (char *)mmap(NULL, kbuf.length, PROT_READ | PROT_WRITE, MAP_SHARED, vfd, kbuf.m.offset);
		if(MFrame[i].start == MAP_FAILED)
		{
			perror("mmap fail");
            return 0;
		}
		
		//缓存入队
		if(ioctl(vfd, VIDIOC_QBUF, &kbuf) < 0)
		{
			perror("qbuf fail");
            return 0;
		}
	}

    return 1;
}

uint8_t video_linux_start(int vfd)
{
	//启动摄像头数据采集
	enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if(ioctl(vfd, VIDIOC_STREAMON, &type)<0)
	{
		perror("start fail");
        return 0;
	}

    return 1;
}

uint8_t video_linux_get_frame(int vfd, FrameBuffer *frame)
{
	//出队
	struct v4l2_buffer readbuf;
	readbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	readbuf.memory = V4L2_MEMORY_MMAP;
	if(ioctl(vfd, VIDIOC_DQBUF, &readbuf) < 0)//取一帧数据
	{
		perror("dqbuf fail");
        return 0;
	}

    memcpy(frame->buf, MFrame[readbuf.index].start, MFrame[readbuf.index].length);
    frame->length = readbuf.length;
	//入队
	if(ioctl(vfd, VIDIOC_QBUF, &readbuf) < 0)
	{
		perror("qbuf fail");
        return 0;
	}

    return 1;
}

uint8_t video_linux_stop(int vfd)
{
	enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	//采集结束
	if(ioctl(vfd, VIDIOC_STREAMOFF, &type)<0)
	{
		perror("stop fail");
        return 0;
	}
	//释放映射空间
	int i=0;
	for(i=0; i<COUNT; i++)
	{
		munmap(MFrame[i].start, MFrame[i].length);
	}

    return 1;
}

void video_linux_destroy(int vfd)
{
	close(vfd);	
}
