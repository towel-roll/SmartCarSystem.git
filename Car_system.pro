QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    backoff.cpp \
    language.cpp \
    main.cpp \
    carsystem.cpp \
    monitor.cpp \
    music.cpp \
    settime.cpp \
    setup.cpp \
    statusbar.cpp \
    v4l2/v4l2.c \
    v4l2/yuv2bgr.c \
    weather.cpp

HEADERS += \
    backoff.h \
    carsystem.h \
    language.h \
    monitor.h \
    music.h \
    settime.h \
    setup.h \
    statusbar.h \
    v4l2/v4l2.h \
    v4l2/yuv2bgr.h \
    weather.h

FORMS += \
    backoff.ui \
    carsystem.ui \
    language.ui \
    monitor.ui \
    music.ui \
    settime.ui \
    setup.ui \
    statusbar.ui \
    weather.ui


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    image.qrc

TRANSLATIONS += \
    Chinese.ts \
    English.ts

DISTFILES +=
