#include "carsystem.h"
#include "ui_carsystem.h"

CarSystem::CarSystem(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::CarSystem)
{
    ui->setupUi(this);

/*****************************天气窗体*************************************/
    //新建一个窗体 weather
    Weather *w = new  Weather(this);
    //创建一项信息
    QListWidgetItem  *it = new QListWidgetItem();
    it->setFlags(it->flags() & ~Qt::ItemIsSelectable);      //消除选中效果
    //把该项信息 设置到 列表中
    ui->WeaTime_lw->addItem(it);
    //设置IT  信息项的大小    (根据窗体大小来设置  it 项的大小)
    it->setSizeHint(w->sizeHint());
    //为该选 信息添加 窗体
    ui->WeaTime_lw->setItemWidget(it,w);

/*********************************状态栏**********************************/
    //新建一个窗体 statusbar
    StatusBar *s = new  StatusBar(this);
    //创建一项信息
    QListWidgetItem  *it_s = new QListWidgetItem();
    it_s->setFlags(it_s->flags() & ~Qt::ItemIsSelectable);      //消除选中效果
    //把该项信息 设置到 列表中
    ui->StatusBar_lw->addItem(it_s);
    //设置IT  信息项的大小    (根据窗体大小来设置  it 项的大小)
    it_s->setSizeHint(s->sizeHint());
    //为该选 信息添加 窗体
    ui->StatusBar_lw->setItemWidget(it_s,s);

/****************************************************************************/
    t_back = new QTimer(this);

    key_fd = ::open("/dev/my_key", O_RDWR);

    if(key_fd<0)
        perror("back_fd error");

    connect(t_back,SIGNAL(timeout()),this,SLOT(back()));

    t_back->start(1000);

    /***************************获取数据库中保存的语言并设置**********************************************/
    QSettings set("soft.ini", QSettings::IniFormat);
    QString str = set.value("language").toString();
    qDebug() << "language is " << str;
    if(str == "Chinese")
        str = "./qm/Chinese.qm";
    else if(str == "English")
        str = "./qm/English.qm";

    //Qt语言家
    QTranslator *lator = new QTranslator(this);
    lator->load(str);
    qApp->installTranslator(lator);
    this->ui->retranslateUi(this);

}

CarSystem::~CarSystem()
{
    t_back->stop();

    delete t_back;

    ::close(key_fd);

    delete ui;

    qDebug()<<"~Carsystem"<<endl;
}

void CarSystem::on_Music_bt_clicked()
{
    Music *music = new Music(this);
    music->show();
    this->hide();
}


void CarSystem::on_Monitor_bt_clicked()
{
    Monitor *monitor = new Monitor(this);
    monitor->show();
    this->hide();
}

void CarSystem::on_Set_up_bt_clicked()
{
    Setup *setup = new Setup(this);
    setup->show();
    this->hide();
}

void CarSystem::changeEvent(QEvent *e)
{
    if(e->type() == QEvent::LanguageChange)
    {
        this->ui->retranslateUi(this);
    }
}

void CarSystem::showEvent(QShowEvent *)
{
    if(flag_event == 1)
    {
        qDebug()<<"Carsystem show"<<endl;
        flag_event = 0;

        //重新打开驱动
        key_fd = ::open("/dev/my_key", O_RDWR);

        if(key_fd<0)
            perror("back_fd error");

        t_back->start(1000);
    }
}

void CarSystem::hideEvent(QHideEvent *)
{
    qDebug()<<"Carsystem hide"<<endl;

    flag_event = 1;

    t_back->stop();

    //关闭驱动
    ::close(key_fd);
}

void CarSystem::back()
{
    int val = -1;
    //读取按键的状态
    int rt = ioctl(key_fd, GEC6818_K4_STA, &val);

    if(rt == 0)
    {
       if(val == 1)
       {
           qDebug()<<"K4 enter"<<endl;

           Backoff *w = new Backoff(this);
           w->show();

           this->hide();
       }
    }
}
