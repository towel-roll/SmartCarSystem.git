#include "statusbar.h"
#include "ui_statusbar.h"

StatusBar::StatusBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StatusBar)
{
    ui->setupUi(this);

    //模拟显示电量
    ui->Battery_pb->setValue(90);

    //定时器显示时间
    time = new QTimer(this);
    //
    connect(time,SIGNAL(timeout()),this,SLOT(show_time()));

    time->start(500);
}

StatusBar::~StatusBar()
{
    time->stop();

    delete time;

    delete ui;

    qDebug()<<"~StatusBar"<<endl;
}

void StatusBar::show_time()
{
    ui->Time_lb->setText(QTime::currentTime().toString("hh:mm"));
}

//按下返回时flag变为1
void StatusBar::on_Return_bt_clicked()
{
    flag_return = 1;
}
