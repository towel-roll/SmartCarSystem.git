#include "monitor.h"
#include "ui_monitor.h"

Monitor::Monitor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Monitor)
{
    ui->setupUi(this);

    /******************************video****************************************/
    t_video = new QTimer(this);
    connect(t_video,SIGNAL(timeout()),this,SLOT(video()));
    t_video->start(100);

    //初始化摄像头设备
    vfd = video_linux_init("/dev/video0", 640, 480);
    video_linux_mmap(vfd);
    //开启摄像头的捕捉画面功能
    video_linux_start(vfd);

    /***************************statusbar**************************************/
    //新建一个窗体 statusbar
    s = new  StatusBar(this);
    //创建一项信息
    QListWidgetItem  *it_s = new QListWidgetItem();
    it_s->setFlags(it_s->flags() & ~Qt::ItemIsSelectable);      //消除选中效果
    //把该项信息 设置到 列表中
    ui->StatusBar_lw->addItem(it_s);
    //设置IT  信息项的大小    (根据窗体大小来设置  it 项的大小)
    it_s->setSizeHint(s->sizeHint());
    //为该选 信息添加 窗体
    ui->StatusBar_lw->setItemWidget(it_s,s);

    t_status = new QTimer(this);
    connect(t_status,SIGNAL(timeout()),this,SLOT(status()));
    t_status->start(500);

    /****************************************************************************/
    t_back = new QTimer(this);

    key_fd = ::open("/dev/my_key", O_RDWR);

    if(key_fd<0)
        perror("back_fd error");

    connect(t_back,SIGNAL(timeout()),this,SLOT(back()));

    t_back->start(1000);

    //子函数调用析构函数
    setAttribute(Qt::WA_DeleteOnClose);
}

Monitor::~Monitor()
{
    t_video->stop();

    //退出摄像头
    video_linux_stop(vfd);
    video_linux_destroy(vfd);

    delete t_video;

    t_status->stop();

    delete t_status;

    t_back->stop();

    delete t_back;

    ::close(key_fd);

    delete s;

    delete ui;

    qDebug()<<"~Monitor"<<endl;
}

void Monitor::status()
{
    if(s->flag_return == 1)
    {
        s->flag_return = 0;
        this->close();
        this->parentWidget()->show();
    }
}

void Monitor::video()
{
    //显示视频流
    //抓取一针图像
    video_linux_get_frame(vfd, &frame);
    uint8_t buf[640*480*3];
    Yuv422ToBgr24(frame.buf, buf, 640, 480);

    QImage image = QImage(buf, 640, 480, QImage::Format_RGB888).rgbSwapped()
            .scaled(ui->Video->size());
    QPixmap map;
    map.convertFromImage(image);

    ui->Video->setPixmap(map);
}

void Monitor::showEvent(QShowEvent *)
{
    if(flag_event == 1)
    {
        qDebug()<<"Monitor show"<<endl;

        flag_event = 0;

        //开启驱动文件夹
        key_fd = ::open("/dev/my_key", O_RDWR);

        if(key_fd<0)
            perror("back_fd error");

        //初始化摄像头设备
        vfd = video_linux_init("/dev/video0", 640, 480);
        video_linux_mmap(vfd);
        //开启摄像头的捕捉画面功能
        video_linux_start(vfd);

        //开启定时器
        t_status->start(500);

        t_video->start(100);

        t_back->start(1000);
    }
}

void Monitor::hideEvent(QHideEvent *)
{
    if(flag_event == 2)
    {
        qDebug()<<"Monitor hide"<<endl;

        flag_event = 1;

        //暂时关掉定时器
        t_status->stop();

        t_video->stop();

        t_back->stop();

        //关掉摄像头
        video_linux_stop(vfd);
        video_linux_destroy(vfd);

        //关闭驱动文件
        ::close(key_fd);
    }
}

void Monitor::back()
{
    int val = -1;
    //读取按键的状态
    int rt = ioctl(key_fd, GEC6818_K4_STA, &val);

    if(rt == 0)
    {
       if(val == 1)
       {
           qDebug()<<"K4 enter"<<endl;

           flag_event = 2;

           this->hide();

           Backoff *w = new Backoff(this);
           w->show();

       }
    }
}

