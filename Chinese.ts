<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Backoff</name>
    <message>
        <location filename="backoff.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="backoff.ui" line="27"/>
        <source>BackoffVideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="backoff.ui" line="51"/>
        <source>距离：</source>
        <translation>距离：</translation>
    </message>
    <message>
        <location filename="backoff.ui" line="72"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CarSystem</name>
    <message>
        <location filename="carsystem.ui" line="14"/>
        <source>CarSystem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="carsystem.ui" line="127"/>
        <source>音乐</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="carsystem.ui" line="181"/>
        <source>行车记录仪</source>
        <translation>行车记录仪</translation>
    </message>
    <message>
        <location filename="carsystem.ui" line="234"/>
        <source>设置</source>
        <translation>设置</translation>
    </message>
</context>
<context>
    <name>Language</name>
    <message>
        <location filename="language.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="language.ui" line="35"/>
        <source>语言</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="language.ui" line="48"/>
        <source>简体中文</source>
        <translation>简体中文</translation>
    </message>
    <message>
        <location filename="language.ui" line="74"/>
        <source>English</source>
        <translation>English</translation>
    </message>
</context>
<context>
    <name>Monitor</name>
    <message>
        <location filename="monitor.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="monitor.ui" line="47"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Music</name>
    <message>
        <location filename="music.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="music.ui" line="224"/>
        <source>顺序播放</source>
        <translation>顺序播放</translation>
    </message>
    <message>
        <location filename="music.ui" line="229"/>
        <source>循环播放</source>
        <translation>循环播放</translation>
    </message>
    <message>
        <location filename="music.ui" line="234"/>
        <source>单曲循环</source>
        <translation>单曲循环</translation>
    </message>
    <message>
        <location filename="music.ui" line="239"/>
        <source>随机播放</source>
        <translation>随机播放</translation>
    </message>
    <message>
        <location filename="music.cpp" line="238"/>
        <source>选择音乐</source>
        <translation>选择音乐</translation>
    </message>
    <message>
        <location filename="music.cpp" line="238"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="music.cpp" line="238"/>
        <source>*.mp3 *.flac *.wma</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetTime</name>
    <message>
        <location filename="settime.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settime.ui" line="50"/>
        <source>设置日期和时间</source>
        <translation>设置日期和时间</translation>
    </message>
    <message>
        <location filename="settime.ui" line="76"/>
        <source>确定</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="settime.ui" line="89"/>
        <source>取消</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Setup</name>
    <message>
        <location filename="setup.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="setup.ui" line="58"/>
        <source>设置</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="setup.ui" line="79"/>
        <source>语言</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="setup.ui" line="100"/>
        <source>时间</source>
        <translation>时间</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="statusbar.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statusbar.ui" line="140"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Weather</name>
    <message>
        <location filename="weather.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="weather.ui" line="41"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="weather.ui" line="128"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="weather.ui" line="149"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="weather.ui" line="172"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="weather.ui" line="193"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="weather.ui" line="214"/>
        <source>Weather</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
