#include "carsystem.h"

#include <QApplication>
#include <QLabel>
#include <QMovie>
#include <QSplashScreen>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QPixmap pixmap(":/background-image/background.jpg"); //绑定一个图片
    QSplashScreen *splash = new QSplashScreen;
    splash->setPixmap(pixmap.scaled(800, 480));
    splash->show(); //显示启动画面

    splash->showMessage("Version:2.0.0.0.2\nCopyright(c)2023 Towel Roll", Qt::AlignHCenter|Qt::AlignBottom, Qt::white);//版本，版权信息

    //显示动画(gif图片)
    QLabel *label = new QLabel(splash);
    QMovie *movie = new QMovie(":/background-image/gif.gif");

    label->setMovie(movie);
    movie->start();

    label->setGeometry(280, 350, 240, 80);
    movie->setScaledSize(label->size());
    label->show();

    QDateTime n=QDateTime::currentDateTime();
    QDateTime now;
    do{
        now=QDateTime::currentDateTime();
        a.processEvents();
    } while (n.secsTo(now)<=3);//3为需要延时的秒数

    CarSystem w;
    w.show();

    splash->finish(&w); //当主窗口启动后，启动画面隐藏
    delete splash;

    return a.exec();
}
