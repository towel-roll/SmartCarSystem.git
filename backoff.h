#ifndef BACKOFF_H
#define BACKOFF_H

#include <QMainWindow>
#include <QTimer>
#include <QEvent>
#include <QMessageBox>
#include <QDebug>

extern "C"{
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "v4l2/v4l2.h"
#include "v4l2/yuv2bgr.h"
}

#define GEC6818_SR04_DISTANCE	_IOR('S', 1, unsigned long)
#define GEC6818_K3_STA		_IOR('K', 2, unsigned long)

namespace Ui {
class Backoff;
}

class Backoff : public QMainWindow
{
    Q_OBJECT

public:
    explicit Backoff(QWidget *parent = nullptr);
    ~Backoff();

private slots:
    void video();

//    void get_distance();

//    void go_home();

    void closeEvent(QCloseEvent *event);

private:
    Ui::Backoff *ui;

    QTimer *t_video;

//    QTimer *t_sr04;

//    QTimer *t_return;

    FrameBuffer frame;

    int vfd;
};

#endif // BACKOFF_H
