#include <linux/module.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/ioport.h>
#include <linux/io.h>
#include <linux/gpio.h>
#include <cfg_type.h>
#include <linux/ioctl.h>
#include <linux/miscdevice.h>		//混杂设备

#define GEC6818_K2_STA		_IOR('K', 1, unsigned long)
#define GEC6818_K3_STA		_IOR('K', 2, unsigned long)
#define GEC6818_K4_STA		_IOR('K', 3, unsigned long)
#define GEC6818_K6_STA		_IOR('K', 4, unsigned long)

#define GEC6818_KALL_STA 	_IOR('K', 5, unsigned long)		//同时按下多个按键

static struct gpio keys_gpios[4] = {
	{ PAD_GPIO_A + 28, GPIOF_IN, "gpioa28" },
	{ PAD_GPIO_B + 30, GPIOF_IN, "gpiob30" },
	{ PAD_GPIO_B + 31, GPIOF_IN, "gpiob31" },
	{ PAD_GPIO_B + 9, GPIOF_IN, "gpiob9" },
};

static int key_open(struct inode *ip, struct file *fp)
{
	printk("key_open \n");
	
	return 0;
}

static int key_close(struct inode *ip, struct file *fp)
{
	printk("key_close \n");
	
	return 0;
}

//long (*unlocked_ioctl) (struct file *, unsigned int, unsigned long);
long key_ioctl(struct file *fp, unsigned int cmd, unsigned long args)
{
	void __user *argp = (void __user *) args;
	
	unsigned int key_val = 0;	//键值
	
	int rt = 0;
	
	int i = 0;
	
	switch(cmd)
	{
		case GEC6818_KALL_STA:
		{
			for(i=0;i<4;i++)
				key_val |= gpio_get_value(keys_gpios[i].gpio)? 0: 1<<i;
		}break;
		
		case GEC6818_K2_STA:
		{
			key_val = gpio_get_value(keys_gpios[0].gpio)? 0:1;
		}break;
		case GEC6818_K3_STA:
		{
			key_val = gpio_get_value(keys_gpios[1].gpio)? 0:1;
		}break;
		case GEC6818_K4_STA:
		{
			key_val = gpio_get_value(keys_gpios[2].gpio)? 0:1;
		}break;
		case GEC6818_K6_STA:
		{
			key_val = gpio_get_value(keys_gpios[3].gpio)? 0:1;
		}break;
	}
	
	rt = copy_to_user(argp,&key_val,4);
	
	if(rt != 0)
		return -EFAULT;
		
	return 0;
}

static struct file_operations key_fops={
	.owner = THIS_MODULE,
	.open = key_open,
	.release = key_close,
	.unlocked_ioctl = key_ioctl,
};

static struct miscdevice key_miscdev={
	.minor = MISC_DYNAMIC_MINOR,	//动态分配次设备号
	.name = "my_key",				//设备名称，/dev/my_key
	.fops = &key_fops,				//文件操作集
};

//入口函数
static int __init gec6818_key_init(void)
{
	int rt = 0;
	
	rt = misc_register(&key_miscdev);
	
	if(rt)
	{
		printk("misc_register fail \n");
		
		return rt;
	}
	
	//由于内核已经包含了gpioe13引脚的gpio申请使用权，必须先释放该引脚的占用
	gpio_free_array(keys_gpios, ARRAY_SIZE(keys_gpios));
	
	rt = gpio_request_array(keys_gpios, ARRAY_SIZE(keys_gpios));
	
	if(rt)
	{
		printk("gpio_request_array fail\n");
		goto err_gpio_request_array;
	}
	
	printk("<3> gec6818 key init \n");
	
	return 0;

err_gpio_request_array:
	gpio_free_array(keys_gpios, ARRAY_SIZE(keys_gpios));
	
	misc_deregister(&key_miscdev);

	return rt;
}

//出口函数
static void __exit gec6818_key_exit(void)
{
	misc_deregister(&key_miscdev);
	
	gpio_free_array(keys_gpios, ARRAY_SIZE(keys_gpios));
	
	printk("<3> gec6818 key exit\n");
} 

//驱动程序的入口:安装驱动的时候，会调用module_init这个函数，这个函数又调用gec6818_key_init
module_init(gec6818_key_init);

//驱动程序的出口：卸载驱动的时候，会调用module_exit这个函数，这个函数又调用gec6818_key_exit
module_exit(gec6818_key_exit);

MODULE_AUTHOR("Towel Roll");
MODULE_DESCRIPTION("gec6818 key deriver");
MODULE_LICENSE("GPL");	//遵循GPL协议